// ==================================================
// 描画オブジェクトクラス
// 
// 描画する全てのオブジェクトはこのクラスを継承する
// ==================================================
class ViewObj {
    // =========================
    // コンストラクタ
    // =========================
    constructor(x, y) {
        // id
        this.id = this.createId();
        // x
        this.x = x;
        // y
        this.y = y;
        // src
        this.src = "";
        // hp
        this.hp = 1;
    }

    // =========================
    // 更新
    // =========================
    update(x, y) {
        // x
        this.x = x;
        // y
        this.y = y;
    }

    // =========================
    // 描画
    // =========================
    view() {
        if (this.hp <= 0) {
            VIEW.deleteVObj(this.id);

            return;
        }

        VIEW.cOrUVObj(this.id, this.src, this.x, this.y);
    }

    // ==================================================
    // IDを生成
    // 
    // ランダム文字列を生成する
    // ==================================================
    createId() {
	    return Math.random().toString(16).substring(2);
    }
}



// ==================================================
// モグラ
// ==================================================
class Mogura extends ViewObj {
    // =========================
    // コンストラクタ
    // =========================
    constructor(pos, hp, speed, maxWaitCnt, score) {
        // 指定されたposの位置に生成
        super(MOGURA_M_LEFT + (MOGURA_INTERVAL + 100) * pos, MOGURA_M_TOP);

        // hp
        this.hp = hp;
        // speed
        this.speed = speed;
        // 最大停止カウント
        this.maxWaitCnt = maxWaitCnt;
        // スコア
        this.score = score;
        // src
        this.src = IMG_DIR_PATH + "mogura.jpg";

        // 状態
        this.state = STATE_MOGURA.PUSH;
        // 停止カウント
        this.waitCnt = 0;
    }

    // =========================
    // (オーバーライド) 更新
    // 
    // 顔を出す
    // =========================
    update() {
        switch (this.state) {
            case STATE_MOGURA.PUSH:
                this.y += this.speed;

                if (MOGURA_RANGE <= this.y - MOGURA_M_TOP) {
                    this.state = STATE_MOGURA.WAIT;
                }

                break;
            case STATE_MOGURA.WAIT:
                this.waitCnt++;

                if (this.maxWaitCnt <= this.waitCnt) {
                    this.state = STATE_MOGURA.PULL;
                }

                break;
            case STATE_MOGURA.PULL:
                this.y -= this.speed;

                if (this.y <= MOGURA_M_TOP) {
                    this.hp = 0;
                }

                break;
            default:
                // 何もしない
                break;
        }
    }
}



// ==================================================
// ハンマー
// ==================================================
class Hammer {
    // =========================
    // コンストラクタ
    // =========================
    constructor() {
        // id
        this.id = "hammer";
        // 通常状態のsrc
        this.srcNormal = IMG_DIR_PATH + "hammer.png";
        // 攻撃状態のsrc
        this.srcAttack = IMG_DIR_PATH + "hammer_attack.png";
        // X
        this.x = HAMMER_X;
        // Y
        this.y = HAMMER_Y;
    }

    update(x, y) {
        if (x == undefined || y == undefined) {
            return;
        }

        this.x = x;
        this.y = y;
    }
}



// ==================================================
// プレイヤー
// ==================================================
class Player {
    // =========================
    // コンストラクタ
    // =========================
    constructor(x, y) {
        this.x = x;
        this.y = y;

        // スコア
        this.score = 0;
        // 状態
        this.state = STATE_PLAYER.NORMAL;
        // 攻撃カウント
        this.cntAtt = 0;
    }

    // =========================
    // 叩く
    // =========================
    attack(x, y) {
        if (x == undefined || y == undefined) {
            return;
        }
        
        if (this.state == STATE_PLAYER.ATTACK) {
            return;
        }

        this.state = STATE_PLAYER.ATTACK;
        this.cntAtt = ATTACK_F_CNT;

        let mogura = getAttackMogura(x, y);

        if (null != mogura) {
            AUDIO.play(AUDIO_TYPE.ATTACK);
            this.score += mogura.score;
            mogura.hp--;
        }
    }

    // =========================
    // 更新
    // =========================
    update() {
        if (this.state == STATE_PLAYER.ATTACK) {
            this.cntAtt--;

            if (this.cntAtt == 0) {
                this.state = STATE_PLAYER.NORMAL;
            }
        }
    }
}



// ==================================================
// シナリオ
// ==================================================
class Sc {
    // =========================
    // モグラを生成
    // 
    // 引数1：pos        左からの位置(0〜4)
    // 引数2：hp         叩いて死ぬ回数
    // 引数3：speed      動く速度
    // 引数4：maxWaitCnt 最大長の待機時間
    // 引数5：score      倒した時のスコア
    // =========================
    createMogura(fCnt) {
        switch(fCnt) {
            // フェーズ1 数:1, 速度:遅い(20), 待機時間:長い(30)
            case 0:
                this.createTargetMogura(1, 20, 30, 100);
                break;
            case 60:
                this.createTargetMogura(1, 20, 30, 100);
                break;
            // フェーズ2 数:2, 速度:遅い(20), 待機時間:長い(30)
            case 120:
                this.createTargetMogura(2, 20, 30, 100);
                break;
            case 180:
                this.createTargetMogura(2, 20, 30, 100);
                break;
            // フェーズ3 数:3, 速度:遅い(30), 待機時間:長い(30)
            case 240:
                this.createTargetMogura(3, 30, 30, 100);
                break;
            case 280:
                this.createTargetMogura(3, 30, 30, 100);
                break;
            // フェーズ4 数:2, 速度:普通(30), 待機時間:普通(20)
            case 320:
                this.createTargetMogura(2, 30, 20, 100);
                break;
            case 360:
                this.createTargetMogura(2, 30, 20, 100);
                break;
            // フェーズ5 数:4, 速度:普通(30), 待機時間:普通(20)
            case 400:
                this.createTargetMogura(4, 30, 20, 100);
                break;
            case 440:
                this.createTargetMogura(4, 30, 20, 100);
                break;
            // フェーズ6 数:2, 速度:早い(40), 待機時間:短い(5)
            case 480:
                this.createTargetMogura(2, 40, 5, 100);
                break;
            case 520:
                this.createTargetMogura(2, 40, 5, 100);
                break;
            // フェーズ7 数:5, 速度:早い(40), 待機時間:短い(5)
            case 560:
                this.createTargetMogura(5, 40, 5, 100);
                break;
            case 600:
                this.createTargetMogura(5, 40, 5, 100);
                break;
            default:
                // 何もしない
                break;
        }
    }

    // =========================
    // 指定されたモグラを生成
    // =========================
    createTargetMogura(num, speed, waitTime, score) {
        let posArr = new Array();

        // 0 〜 MOGURA_NUMまでの数字の配列を生成
        for (let j = 0;j < MOGURA_NUM; j++) {
            posArr.push(j);
        }

        // numの数だけモグラを生成
        for (let i = 0;i < num; i++) {
            // ランダムにidxを取得
            let idx = Math.floor(Math.random() * posArr.length);

            // モグラを生成
            V_OBJ_ARR.push(new Mogura(posArr[idx], 1, speed, waitTime, score));

            // idxの要素を削除
            posArr.splice(idx, 1);
        }
    }
}
