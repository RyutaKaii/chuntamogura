// ==================================================
// ちゅんたもぐらたたき
// 
// ワニワニパニックを模倣したもぐらたたきゲーム
// ==================================================



// ==================================================
// 定数
// ==================================================
// 画像ディレクトリパス
const IMG_DIR_PATH = "./imgs/";
// 音声ディレクトリパス
const AUDIO_DIR_PATH = "./audio/";
// 1フレームあたりの時間
const INTERVAL_TIME = 0.1;
// 攻撃中画像表示フレーム数
const ATTACK_F_CNT = 3;
// 当たり判定距離
const ATARI_RANGE = 100;
// 最大フレームカウント
const MAX_FRAME_CNT = 650;

// モグラの横幅
const MOGURA_W = 100;
// モグラの縦幅
const MOGURA_H = 200;
// モグラの数
const MOGURA_NUM = 5;
// モグラの間隔
const MOGURA_INTERVAL = 12;
// モグラの上マージン
const MOGURA_M_TOP = 50;
// モグラの左マージン
const MOGURA_M_LEFT = 30;
// モグラの長さ
const MOGURA_RANGE = 120;

// スコアのhtmlのID
const SCORE_HTML_ID = "score";
// スコアの上マージン
const SCORE_M_TOP = 20;
// スコアの左マージン
const SCORE_M_LEFT = 20;

// ハンマーの初期位置X
const HAMMER_X = 300;
// ハンマーの初期位置Y
const HAMMER_Y = 400;
// ハンマーの横幅
const HAMMER_W = 150;
// ハンマーの縦幅
const HAMMER_H = 150;

// ゲームを描画するhtmlのID
const HTML_ID = "game_area";



// ==================================================
// グローバル変数
// ==================================================
// 表示クラス
let VIEW;
// シナリオ
let SC;
// プレイヤー
let PLAYER;
// ハンマー
let HAMMER;
// 描画オブジェクト配列
let V_OBJ_ARR;



// ==================================================
// モグラの状態
// ==================================================
var STATE_MOGURA = {PUSH: "PUSH", WAIT: "WAIT", PULL: "PULL"};



// ==================================================
// プレイヤーの状態
// ==================================================
var STATE_PLAYER = {NORMAL: "NORMAL", ATTACK: "ATTACK"};



// ==================================================
// 結果画面メッセージ
// ==================================================
var RESULT_MSG = {EXCELLENT: "もうかんぺきだね！！", GREAT: "よくできました！！！", NORMAL: "やったね！おめでとう", BAD: "あとちょっとだね！！", BOO: "つぎはがんばろう！！"};



// ==================================================
// 音声の種類
// ==================================================
var AUDIO_TYPE = {BG: "BG", ENTER: "ENTER", ATTACK: "ATTACK", SUCCESS: "SUCCESS", FAIL: "FAIL"};



// ==================================================
// ロード時の処理
// ==================================================
function fncOnload() {
    VIEW = new View();
    SC = new Sc();
    PLAYER = new Player();
    HAMMER = new Hammer();
    V_OBJ_ARR = new Array();
    AUDIO = new AudioClass();

    VIEW.viewTitle();
}



// ==================================================
// (非同期処理) プレイ時の処理
// ==================================================
async function play() {
    VIEW.viewReady();
    await sleep(2);

    VIEW.viewGo();
    await sleep(2);

    document.body.addEventListener("mousemove", function(e) {
        HAMMER.update(e.pageX, e.pageY);
    });

    document.body.addEventListener("click", function(e) {
        PLAYER.attack(e.pageX, e.pageY);
    });

    VIEW.viewPlay();

    let fCnt = 0;

    while (true) {
        SC.createMogura(fCnt);

        update();

        view();

        finallize();
        
        await sleep(INTERVAL_TIME);

        fCnt++;

        if (MAX_FRAME_CNT < fCnt) {
            break;
        }
    }

    VIEW.viewResult();
}



// ==================================================
// 更新
// ==================================================
function update() {
    // 描画オブジェクト
	for (var i = 0;i < V_OBJ_ARR.length; i++) {
		V_OBJ_ARR[i].update();
    }
    
    // プレイヤー
    PLAYER.update();
}



// ==================================================
// ファイナライズ
// ==================================================
function finallize() {
    // 死んでいるオブジェクトを削除
	var delIdxArr = new Array();
	
	for (var i = 0;i < V_OBJ_ARR.length; i++) {
		if (V_OBJ_ARR[i].hp <= 0) {
			delIdxArr.push(i);
		}
	}
	
	for (var j = 0;j < delIdxArr.length; j++) {
		V_OBJ_ARR.splice(delIdxArr[j] , 1);
	}
}



// ==================================================
// 描画
// ==================================================
function view() {
    // 描画オブジェクト
	for (var i = 0;i < V_OBJ_ARR.length; i++) {
		V_OBJ_ARR[i].view();
    }
    
    // ハンマー
    VIEW.cOrUHammer();

    // スコア
    VIEW.cOrUVTxt(SCORE_HTML_ID, PLAYER.score, SCORE_M_LEFT, SCORE_M_TOP);
}



// ==================================================
// 指定されたオブジェクトを取得
// ==================================================
function getTargetObj(targetName) {
    for (let i = 0;i < V_OBJ_ARR.length; i++) {
        if (targetName == V_OBJ_ARR[i].constructor.name) {
            return V_OBJ_ARR[i];
        }
    }
    
    return null;
}



// ==================================================
// 指定された座標上にいる1匹のモグラを取得
// ==================================================
function getAttackMogura(x, y) {
    for (let i = 0;i < V_OBJ_ARR.length; i++) {
        if ("Mogura" == V_OBJ_ARR[i].constructor.name) {
            // モグラの頭の中心との距離を利用する
            if (getRange(x, y, V_OBJ_ARR[i].x + (MOGURA_W / 2), V_OBJ_ARR[i].y + MOGURA_H) < ATARI_RANGE) {
                return V_OBJ_ARR[i];
            }
        }
    }
    
    return null;
}



// ==================================================
// 距離を取得
// ==================================================
function getRange(fromX, fromY, toX, toY) {
    var x = toX - fromX;
    var y = toY - fromY;
    
	return Math.sqrt( (x * x) + (y * y) );
}



// ==================================================
// 指定された時間(s)待機する
// ==================================================
function sleep(sec) {
    return new Promise(resolve => setTimeout(resolve, sec * 1000));
}



// ==================================================
// 結果メッセージを取得
// ==================================================
function getResultMsg(score) {
    if (3000 <= score) {
        return RESULT_MSG.EXCELLENT;
    } else if (2000 <= score) {
        return RESULT_MSG.GREAT;
    } else if (1000 <= score) {
        return RESULT_MSG.NORMAL;
    } else if (500 <= score) {
        return RESULT_MSG.BAD;
    }

    return RESULT_MSG.BOO;
}
