// ==================================================
// 表示クラス
// ==================================================
class View {
    // =========================
    // タイトル画面描画
    // =========================
    viewTitle() {
        this.viewInit();

        this.setBg("bg_title.jpg");

        let startBtn = document.createElement("input");
        startBtn.setAttribute("type", "button");
        startBtn.setAttribute("class", "start_btn");
        startBtn.setAttribute("value", "すたーと");
        startBtn.setAttribute("onclick", "AUDIO.play(AUDIO_TYPE.ENTER);VIEW.viewExp();");

        let gameArea = document.getElementById(HTML_ID);
        gameArea.appendChild(startBtn);
    }

    // =========================
    // 説明画面描画
    // =========================
    viewExp() {
        this.viewInit();

        AUDIO.playBg();

        this.setBg("bg_exp.jpg");

        let nextBtn = document.createElement("input");
        nextBtn.setAttribute("type", "button");
        nextBtn.setAttribute("class", "next_btn");
        nextBtn.setAttribute("value", "つぎへ");
        nextBtn.setAttribute("onclick", "AUDIO.play(AUDIO_TYPE.ENTER);play();");

        let gameArea = document.getElementById(HTML_ID);
        gameArea.appendChild(nextBtn);
    }

    // =========================
    // Ready画面描画
    // =========================
    viewReady() {
        this.viewInit();

        this.setBg("bg_ready.jpg");
    }

    // =========================
    // Go画面描画
    // =========================
    viewGo() {
        this.viewInit();

        this.setBg("bg_go.jpg");
    }

    // =========================
    // Play画面描画
    // =========================
    viewPlay() {
        this.viewInit();

        var image = document.createElement("img");
        image.setAttribute("id", "dai");
        document.getElementById(HTML_ID).appendChild(image);
        image.setAttribute("src", IMG_DIR_PATH + "bg_play.jpg");
    }

    // =========================
    // 結果画面描画
    // =========================
    viewResult() {
        this.viewInit();

        AUDIO.stopBg();
        AUDIO.playResult(PLAYER.score);

        this.setBg("bg_result.jpg");

        var p_score = document.createElement("p");
		p_score.setAttribute("id", "result_score");
        p_score.innerHTML = String(PLAYER.score) + "てん";

        var p_msg = document.createElement("p");
		p_msg.setAttribute("id", "result_msg");
        p_msg.innerHTML = getResultMsg(PLAYER.score);

        let returnBtn = document.createElement("input");
        returnBtn.setAttribute("type", "button");
        returnBtn.setAttribute("class", "return_btn");
        returnBtn.setAttribute("value", "もういっかい");
        returnBtn.setAttribute("onclick", "location.reload();");

        let gameArea = document.getElementById(HTML_ID);
        gameArea.appendChild(p_score);
        gameArea.appendChild(p_msg);
        gameArea.appendChild(returnBtn);
    }

    // =========================
    // 画面初期化
    // =========================
    viewInit() {
        let gameArea = document.getElementById(HTML_ID);
        gameArea.textContent = null;
        gameArea.style.backgroundImage = null;
    }

    // =========================
    // 背景画像を設定
    // =========================
    setBg(name) {
        let gameArea = document.getElementById(HTML_ID);
        gameArea.style.backgroundImage = "url(" + IMG_DIR_PATH + name;
    }

    // =========================
    // ハンマー要素を生成または更新
    // =========================
    cOrUHammer() {
        var image;
        
        if (document.getElementById(HAMMER.id) == null) {
		    // オブジェクトが存在しない場合は新規作成
		    image = document.createElement("img");
		    image.setAttribute("id", HAMMER.id);
            document.getElementById(HTML_ID).appendChild(image);
            image.style.position = "absolute";
	    } else {
		    image = document.getElementById(HAMMER.id);
        }

        // プレイヤーの状態によってsrcを変更
        if (PLAYER.state == STATE_PLAYER.NORMAL) {
            image.setAttribute("src", HAMMER.srcNormal);

            // プレイヤーが通常状態の場合は位置を更新
            // 叩く部分の中心がマウスカーソルのポインタの位置になるようにする
            image.style.left = HAMMER.x - (HAMMER_W / 4) + "px";
	        image.style.top = HAMMER.y - (HAMMER_H / 2) - 30 + "px";
        } else {
            if (PLAYER.cntAtt == ATTACK_F_CNT - 1) {
                image.setAttribute("src", HAMMER.srcAttack);
            }
        }
    }

    // =========================
    // 描画要素を生成または更新
    // =========================
    cOrUVObj(id, src, x, y) {
        var image;
        
        if (document.getElementById(id) == null) {
		    // オブジェクトが存在しない場合は新規作成
		    image = document.createElement("img");
		    image.setAttribute("id", id);
		    document.getElementById(HTML_ID).appendChild(image);
            image.setAttribute("src", src);
            image.setAttribute("class", "view_obj");
            
	        // 位置設定
	        image.style.position = "absolute";
	    } else {
		    image = document.getElementById(id);
        }
        
	    image.style.left = x + "px";
	    image.style.top = y + "px";
    }

    // =========================
    // 描画テキストを生成または更新
    // =========================
    cOrUVTxt(id, txt, x, y) {
        var span;
        
        if (document.getElementById(id) == null) {
		    // オブジェクトが存在しない場合は新規作成
		    span = document.createElement("span");
		    span.setAttribute("id", id);
		    document.getElementById(HTML_ID).appendChild(span);
            
	        // 位置設定
	        span.style.position = "absolute";
	    } else {
		    span = document.getElementById(id);
	    }
        
        span.innerHTML = txt;
	    span.style.left = x + "px";
	    span.style.top = y + "px";
    }

    // =========================
    // 描画要素を削除
    // =========================
    deleteVObj(id) {
	    if (document.getElementById(id) != null) {
		    var element = document.getElementById(id);
		    element.parentNode.removeChild(element);
	    }
    }
}
