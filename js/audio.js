// ==================================================
// オーディオクラス
// ==================================================
class AudioClass {
    // =========================
    // コンストラクタ
    // 
    // 全ての音源をロードする
    // =========================
    constructor() {
        // バックグラウンドBGM
        this.bg = new Audio(AUDIO_DIR_PATH + "bg.mp3");
        // 決定ボタン押下時
        this.enter = new Audio(AUDIO_DIR_PATH + "enter.mp3");
        // 攻撃時
        this.attack = new Audio(AUDIO_DIR_PATH + "attack.mp3");
        // 成功時
        this.success = new Audio(AUDIO_DIR_PATH + "success.mp3");
        // 失敗時
        this.fail = new Audio(AUDIO_DIR_PATH + "fail.mp3");
    }

    // =========================
    // 再生
    // =========================
    play(audioType) {
        var audio = null;

        switch (audioType) {
            case AUDIO_TYPE.BG:
                audio = this.bg;
                audio.loop = true;
                audio.volume = 0.2;
                break;
            case AUDIO_TYPE.ENTER:
                audio = this.enter;
                break;
            case AUDIO_TYPE.ATTACK:
                audio = this.attack;
                break;
            case AUDIO_TYPE.SUCCESS:
                audio = this.success;
                break;
            case AUDIO_TYPE.FAIL:
                audio = this.fail;
                break;
            default:
                // 何もしない
                return;
        }

        audio.currentTime = 0;
        audio.play();
    }

    // =========================
    // 背景音楽再生
    // =========================
    playBg() {
        this.play(AUDIO_TYPE.BG);
    }

    // =========================
    // 背景音楽停止
    // =========================
    stopBg() {
        this.bg.pause();
    }

    // =========================
    // 結果音楽停止
    // =========================
    playResult(score) {
        if (500 <= score) {
            this.play(AUDIO_TYPE.SUCCESS);
        } else {
            this.play(AUDIO_TYPE.FAIL);
        }
    }
}
